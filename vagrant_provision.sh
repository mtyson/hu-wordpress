#!/usr/bin/env bash

sudo apt-get -y update
#sudo apt-get -y upgrade

# web server stuff
sudo apt-get install -y vim
sudo apt-get install -y apache2
sudo apt-get install -y php5
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password donuts'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password donuts'
sudo apt-get install -y mysql-server libapache2-mod-auth-mysql php5-mysql
sudo apt-get install -y php5 libapache2-mod-php5 php5-mcrypt php5-mysql
sudo apt-get install php5-curl
sudo mysql_install_db

# setup symlinks
if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant/wordpress /var/www
fi

# setup hosts file
VHOST=$(cat <<EOF
<VirtualHost *:80>

    DocumentRoot /var/www
    <Directory /var/www>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Order allow,deny
        allow from all
    </Directory>

     ErrorLog ${APACHE_LOG_DIR}/wp_error.log
     CustomLog ${APACHE_LOG_DIR}/wp_access.log combined
</VirtualHost>
EOF
)

sudo rm /etc/apache2/sites-available/000-default
echo "${VHOST}" > /etc/apache2/sites-available/000-default.conf
sudo ln -s /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-enabled/000-default.conf

# setup database
mysql -u root -pdonuts -e "CREATE DATABASE wordpress"

# enable mod_rewrite
sudo a2enmod rewrite
sudo a2enmod expires
sudo a2enmod headers

# restart apache
service apache2 restart