<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

if (isset($_SERVER["HTTP_X_FORWARDED_PROTO"] ) && "https" == $_SERVER["HTTP_X_FORWARDED_PROTO"] ) {
  $_SERVER["HTTPS"] = "on";
}

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'donuts');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`&(<o5pu31TIhBUYjZp#nq6z[S B-czeUi;}nNkS-+D&a}l)YkOt9v !GCB:6lE<');
define('SECURE_AUTH_KEY',  'f%^;-F[2%#[:9rdGro!l9?_(;*RN2]Y2A1vXU;tyBwglP6K?1!H`F%k>,/i.Vj|(');
define('LOGGED_IN_KEY',    'ye-LWAG!A`@|Ydt|4L%p#!U+@j8*7lZmoW+w+VW)u}g5jaM>spH%SL5^qQs+u.bB');
define('NONCE_KEY',        'YGn9.hq/N5R_jRMShA!.4X7&in-caJQM.M,jud%LedA^.F0U&R*{`Y9zFg/`Zn2^');
define('AUTH_SALT',        'SqAjM8Syf.WwmBr[+MS*!;TZ~)[-yJ3~8gLUuZ8DhzK uOQi%3]X6E!h^kjAe;$u');
define('SECURE_AUTH_SALT', '=U1Y=Zde{>zj/S73hI576#z1UZbGNrnY/d(4rc)/bNqBYHkVxGk^%!tX@IHV02jy');
define('LOGGED_IN_SALT',   '5wvr1#yW0^y9Q@D!lp)i#DpgPVaR5(2610^r`Z6b< HFPCbD[z&]_fCP>d>n~M],');
define('NONCE_SALT',       'yFqYqPf& )!:[-}g!gEjP@2z63fXG}x BBT5V]<J_tQF-M=cS}JBr??X?D6qLD02');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
